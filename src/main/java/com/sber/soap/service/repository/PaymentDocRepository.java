package com.sber.soap.service.repository;

import com.sber.soap.service.PaymentDoc;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages data for soap service
 */
@Repository
public class PaymentDocRepository {
    private final Map<Integer, PaymentDoc> paymentDocs = new HashMap<>();

    /**
     * Saves new payment doc to application memory. Checks if it's already exists.
     *
     * @param paymentDoc doc to be saved
     * @return created doc
     */
    public PaymentDoc createPaymentDoc(PaymentDoc paymentDoc) {
        if (paymentDocs.containsKey(paymentDoc.getId())) {
            throw new RuntimeException("ID already exists. To replace payment doc first delete the existing.");
        }

        paymentDocs.put(paymentDoc.getId(), paymentDoc);

        return paymentDocs.get(paymentDoc.getId());
    }

    /**
     * Retrieves payment doc from application memory
     *
     * @param id id as key
     * @return found payment doc
     */
    public PaymentDoc getPaymentDoc(Integer id) {
        return paymentDocs.get(id);
    }

    /**
     * Removes payment doc by id
     *
     * @param id id as key
     * @return removed doc or null in case it was not found
     */
    public PaymentDoc deletePaymentDoc(Integer id) {
        return paymentDocs.remove(id);

    }

    /**
     * Retrieves all payment docs from application memory
     *
     * @return list of all saved payment docs
     */
    public List<PaymentDoc> getAllPaymentDoc() {
        return new ArrayList<>(paymentDocs.values());
    }

    /**
     * Checks if such id is present and updates payment doc in this case
     *
     * @param paymentDoc new value of payment doc
     * @return updated payment doc
     */
    public PaymentDoc updatePaymentDoc(PaymentDoc paymentDoc) {
        if (!paymentDocs.containsKey(paymentDoc.getId())) {
            throw new RuntimeException("Payment document with such id does not exist.");
        }

        paymentDocs.put(paymentDoc.getId(), paymentDoc);
        return paymentDocs.get(paymentDoc.getId());
    }
}
