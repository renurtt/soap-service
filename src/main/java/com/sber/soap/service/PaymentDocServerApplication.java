package com.sber.soap.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentDocServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentDocServerApplication.class, args);
    }
}