package com.sber.soap.service.web;

import com.sber.soap.service.*;
import com.sber.soap.service.service.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

/**
 * Defines endpoints for soap service
 */
@Endpoint
public class PaymentDocEndpoint {
    private static final String NAMESPACE_URI = "http://sber.com/soap/service";

    private final PaymentDocService paymentDocService;

    public PaymentDocEndpoint(PaymentDocService paymentDocService) {
        this.paymentDocService = paymentDocService;
    }

    /**
     * Creates and saves new payment doc.
     *
     * @param request new payment doc
     * @return created payment doc
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "createPaymentDocRequest")
    @ResponsePayload
    public CreatePaymentDocResponse createPaymentDoc(@RequestPayload CreatePaymentDocRequest request) {
        return paymentDocService.createPaymentDoc(request);
    }

    /**
     * Retrieves payment doc by id
     *
     * @param request provides id to look for by
     * @return found payment doc
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPaymentDocRequest")
    @ResponsePayload
    public GetPaymentDocResponse getPaymentDoc(@RequestPayload GetPaymentDocRequest request) {
        return paymentDocService.getPaymentDoc(request);
    }

    /**
     * Removes payment doc by id
     *
     * @param request provides id of doc to be removed
     * @return status ("OK"/"NOT FOUND")
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePaymentDocRequest")
    @ResponsePayload
    public DeletePaymentDocResponse deletePaymentDoc(@RequestPayload DeletePaymentDocRequest request) {
        return paymentDocService.deletePaymentDoc(request);
    }

    /**
     * Retrieves all existing payment docs
     *
     * @return string of concatenated payment-doc objects
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPaymentDocRequest")
    @ResponsePayload
    public GetAllPaymentDocResponse getAllPaymentDoc() {
        return paymentDocService.getAllPaymentDoc();
    }

    /**
     * Updates existing payment doc
     *
     * @return updated payment doc
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updatePaymentDocRequest")
    @ResponsePayload
    public UpdatePaymentDocResponse updatePaymentDoc(@RequestPayload UpdatePaymentDocRequest request) {
        return paymentDocService.updatePaymentDoc(request);
    }
}