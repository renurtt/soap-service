package com.sber.soap.service.service;

import com.sber.soap.service.*;
import com.sber.soap.service.repository.*;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Completes operations for soap service
 */
@Service
public class PaymentDocService {
    private PaymentDocRepository paymentDocRepository;

    public PaymentDocService(PaymentDocRepository paymentDocRepository) {
        this.paymentDocRepository = paymentDocRepository;
    }

    /**
     * Creates and saves payment doc
     *
     * @param request payment doc with both account IDs containing strictly 10 symbols
     * @return created payment doc
     */
    public CreatePaymentDocResponse createPaymentDoc(CreatePaymentDocRequest request) {
        CreatePaymentDocResponse response = new CreatePaymentDocResponse();
        PaymentDoc paymentDoc = request.getPaymentDoc();
        if (!paymentDoc.getRecipientAccount().matches("^[a-zA-Z0-9]{10}$") ||
                !paymentDoc.getWriteOffAccount().matches("^[a-zA-Z0-9]{10}$")) {
            throw new RuntimeException("Accounts length was not 10 or forbidden symbols were used.");
        }

        response.setCreatedPaymentDoc(paymentDocRepository.createPaymentDoc(paymentDoc));
        return response;
    }

    /**
     * Retrieves payment doc by id
     *
     * @param request provides id to look for by
     * @return found payment doc
     */
    public GetPaymentDocResponse getPaymentDoc(GetPaymentDocRequest request) {
        GetPaymentDocResponse response = new GetPaymentDocResponse();
        response.setRequestedPaymentDoc(paymentDocRepository.getPaymentDoc(request.getId()));
        return response;
    }

    /**
     * Removes payment doc by id
     *
     * @param request provides id of doc to be removed
     * @return status ("OK"/"NOT FOUND") on removal status provided by repository
     */
    public DeletePaymentDocResponse deletePaymentDoc(DeletePaymentDocRequest request) {
        DeletePaymentDocResponse response = new DeletePaymentDocResponse();
        if (paymentDocRepository.deletePaymentDoc(request.getId()) != null) {
            response.setStatus("OK");
        } else {
            response.setStatus("Not found");
        }

        return response;
    }

    /**
     * Retrieves all existing payment docs
     *
     * @return string of concatenated payment-doc objects (properties are split by comas, docs by semicolons)
     */
    public GetAllPaymentDocResponse getAllPaymentDoc() {
        GetAllPaymentDocResponse response = new GetAllPaymentDocResponse();
        List<PaymentDoc> paymentDocs = paymentDocRepository.getAllPaymentDoc();
        StringBuffer stringBuffer = new StringBuffer();
        for (PaymentDoc paymentDoc :
                paymentDocs) {
            stringBuffer.append(paymentDoc.getId());
            stringBuffer.append(",");
            stringBuffer.append(paymentDoc.getPurpose());
            stringBuffer.append(",");
            stringBuffer.append(paymentDoc.getAmount());
            stringBuffer.append(",");
            stringBuffer.append(paymentDoc.getWriteOffAccount());
            stringBuffer.append(",");
            stringBuffer.append(paymentDoc.getRecipientAccount());
            stringBuffer.append(";");
        }
        response.setPaymentDocs(stringBuffer.toString());
        return response;
    }

    /**
     * Checks input updated doc for validity
     *
     * @param request new value of payment doc
     * @return updated payment doc
     */
    public UpdatePaymentDocResponse updatePaymentDoc(UpdatePaymentDocRequest request) {
        UpdatePaymentDocResponse response = new UpdatePaymentDocResponse();
        PaymentDoc paymentDoc = request.getPaymentDoc();
        if (!paymentDoc.getRecipientAccount().matches("^[a-zA-Z0-9]{10}$") ||
                !paymentDoc.getWriteOffAccount().matches("^[a-zA-Z0-9]{10}$")) {
            throw new RuntimeException("Accounts length was not 10 or forbidden symbols were used.");
        }

        response.setUpdatedPaymentDoc(paymentDocRepository.updatePaymentDoc(paymentDoc));
        return response;
    }
}
